//import express
const express = require('express');

const app = express();

const port = 3000;

//enable server to read JSON as JS
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//landing page
app.get('/', (req, res) => {
	res.send('Hello World')
});

//post request
app.post('/hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
});

//register page
let users = [];

app.post('/register', (req, res) => {
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered.`)
	} else {
		res.send('Please input BOTH username and password.')
	}
});

//change password page
let message;

app.put('/change-password', (req, res) => {
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`;

			break;
		} else {
			message = 'User does not exist.';
		}
	}
	res.send(message);
});

app.listen(port, () => console.log(`Server is running at port ${port}`));